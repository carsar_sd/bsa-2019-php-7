<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entity\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'quantity' => $quantity = $faker->numberBetween(1, 10),
        'price'    => $price    = $faker->numberBetween(100, 10000),
        'discount' => $discount = $faker->numberBetween(1, 50),
        'amount'   => $quantity * $price * (1 - $discount/100),
        'product_id' => (mt_rand(1, 50) + mt_rand(1, 150)),
    ];
});
