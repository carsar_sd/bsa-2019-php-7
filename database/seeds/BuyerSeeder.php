<?php

use Illuminate\Database\Seeder;

class BuyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Entity\Buyer::class, 10)->create()
            ->each(function ($buyer) {
                $buyer->orders()
                    ->saveMany(
                        factory(App\Entity\Order::class, mt_rand(1, 10))
                            ->make(['buyer_id' => null])
                    )
                    ->each(function ($order) {
                        $order->order_items()->saveMany(
                            factory(App\Entity\OrderItem::class, mt_rand(1, 10))
                                ->make(['order_id' => null])
                        );
                    });
            });
    }
}
