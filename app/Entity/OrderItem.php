<?php

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderItem
 *
 * @package App\Entity
 * @property int $id
 * @property int $product_id
 * @property int $order_id
 * @property int $quantity
 * @property int $price
 * @property int $discount
 * @property float $amount
 * @property Order $order
 * @property Product $product
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class OrderItem extends Model
{
    protected $table = 'order_items';
    protected $primaryKey = 'id';
    protected $attributes = [];
    protected $fillable   = ['order_id', 'product_id', 'price', 'quantity', 'discount', 'amount'];
    protected $guarded    = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getProductId():int
    {
        return $this->product_id;
    }

    /**
     * @return int
     */
    public function getOrderId():int
    {
        return $this->order_id;
    }

    /**
     * @return int
     */
    public function getQuantity():int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getPrice():int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getDiscount():int
    {
        return $this->discount;
    }

    /**
     * @return float
     */
    public function getAmount():float
    {
        return $this->amount;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updated_at;
    }
}
