<?php

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Buyer
 * @package App\Entity
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $country
 * @property string $city
 * @property string $address
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Buyer extends Model
{
    /**
     * @var string
     */
    protected $table = 'buyers';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'buyer_id', 'id');
    }

    /**
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName():string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurName():string
    {
        return $this->surname;
    }

    /**
     * @return string
     */
    public function getCountry():string
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getCity():string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getAddress():string
    {
        return $this->address;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updated_at;
    }
}
