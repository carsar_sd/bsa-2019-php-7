<?php

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @package App\Entity
 * @property int $id
 * @property int $seller_id
 * @property string $name
 * @property int $price
 * @property int $available
 * @property Seller $seller
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Product extends Model
{
    protected $table      = 'products';
    protected $primaryKey = 'id';
    protected $attributes = ['available' => false];
    protected $fillable   = ['name', 'price', 'seller_id'];
    protected $guarded    = ['available'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }

    /**
     * @return Seller
     */
    public function getSeller(): Seller
    {
        return $this->seller;
    }

    /**
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSellerId():int
    {
        return $this->seller_id;
    }

    /**
     * @return string
     */
    public function getName():string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPrice():int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getAvailable():int
    {
        return $this->available;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updated_at;
    }
}
