<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Order;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class OrderRepository
{
    /**
     * @param array $fields
     *
     * @return Order
     */
    public function create(array $fields): Order
    {
        return Order::create($fields);
    }

    /**
     * @param int $id
     * @return Order
     * @throws ModelNotFoundException
     */
    public function getById(int $id): Order
    {
        return Order::findOrFail($id);
    }

    /**
     * @param Order $order
     *
     * @return Order
     */
    public function save(Order $order): Order
    {
        $order->save();

        return $order;
    }

    /**
     * @param Order $order
     *
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Order $order): ?bool
    {
        return $order->delete();
    }
}
