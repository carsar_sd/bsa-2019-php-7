<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\OrderItem;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class OrderItemRepository
{
    /**
     * @param array $fields
     *
     * @return OrderItem
     */
    public function create(array $fields): OrderItem
    {
        return OrderItem::create($fields);
    }

    /**
     * @param int $id
     * @return OrderItem
     * @throws ModelNotFoundException
     */
    public function getById(int $id): OrderItem
    {
        return OrderItem::findOrFail($id);
    }

    /**
     * @param OrderItem $order_item
     *
     * @return OrderItem
     */
    public function save(OrderItem $order_item): OrderItem
    {
        $order_item->save();

        return $order_item;
    }

    /**
     * @param OrderItem $order_item
     *
     * @return bool|null
     * @throws \Exception
     */
    public function delete(OrderItem $order_item): ?bool
    {
        return $order_item->delete();
    }
}
