<?php

declare(strict_types=1);

namespace App\Action;

use App\Entity\OrderItem;

final class AddOrderItemResponse
{
    /**
     * @var OrderItem
     */
    private $order_item;

    /**
     * AddOrderItemResponse constructor.
     *
     * @param OrderItem $order_item
     */
    public function __construct(OrderItem $order_item)
    {
        $this->order_item = $order_item;
    }

    /**
     * @return OrderItem
     */
    public function getOrderItem(): OrderItem
    {
        return $this->order_item;
    }
}
