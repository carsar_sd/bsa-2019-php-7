<?php

declare(strict_types=1);

namespace App\Action;

use App\Entity\Order;

final class AddOrderResponse
{
    /**
     * @var Order
     */
    private $order;

    /**
     * AddOrderResponse constructor.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }
}
