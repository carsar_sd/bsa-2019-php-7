<?php

declare(strict_types=1);

namespace App\Action;

use App\Entity\OrderItem;
use App\Repository\OrderItemRepository;

final class AddOrderItemAction
{
    /**
     * @var OrderItemRepository
     */
    private $orderItemRepository;

    /**
     * AddOrderItemAction constructor.
     *
     * @param OrderItemRepository $orderItemRepository
     */
    public function __construct(OrderItemRepository $orderItemRepository)
    {
        $this->orderItemRepository = $orderItemRepository;
    }

    /**
     * @param AddOrderItemRequest $request
     *
     * @return AddOrderItemResponse
     */
    public function execute(AddOrderItemRequest $request): AddOrderItemResponse
    {
        $order_item = new OrderItem();
        $order_item->order_id   = $request->getOrderId();
        $order_item->product_id = $request->getProductId();
        $order_item->quantity   = $request->getQuantity();
        $order_item->price      = $order_item->getProduct()->getPrice();
        $order_item->discount   = $request->getDiscount();
        $order_item->amount     = $order_item->quantity * $order_item->price * (1 - $order_item->discount/100);
        $order_item = $this->orderItemRepository->save($order_item);

        return new AddOrderItemResponse($order_item);
    }
}
