<?php

declare(strict_types=1);

namespace App\Action;

final class AddOrderItemRequest
{
    /**
     * @var int
     */
    private $order_id;
    /**
     * @var int
     */
    private $product_id;
    /**
     * @var int
     */
    private $quantity;
    /**
     * @var int
     */
    private $price;
    /**
     * @var int
     */
    private $discount;
    /**
     * @var float
     */
    private $amount;

    /**
     * AddOrderItemRequest constructor.
     *
     * @param int   $order_id
     * @param int   $product_id
     * @param int   $quantity
     * @param int   $price
     * @param int   $discount
     * @param float $amount
     */
    public function __construct(int $order_id, int $product_id, int $quantity, int $price, int $discount, float $amount)
    {
        $this->order_id   = $order_id;
        $this->product_id = $product_id;
        $this->quantity   = $quantity;
        $this->price      = $price;
        $this->discount   = $discount;
        $this->amount     = $amount;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->order_id;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->product_id;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getDiscount(): int
    {
        return $this->discount;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
}
