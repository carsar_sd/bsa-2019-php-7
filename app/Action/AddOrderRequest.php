<?php

declare(strict_types=1);

namespace App\Action;

final class AddOrderRequest
{
    /**
     * @var int
     */
    private $buyer_id;

    /**
     * AddOrderRequest constructor.
     *
     * @param int $buyer_id
     */
    public function __construct(int $buyer_id)
    {
        $this->buyer_id = $buyer_id;
    }

    /**
     * @return int
     */
    public function getBuyerId(): int
    {
        return $this->buyer_id;
    }
}
