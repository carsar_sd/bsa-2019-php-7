<?php

declare(strict_types=1);

namespace App\Action;

use App\Entity\Order;
use App\Repository\OrderRepository;

final class AddOrderAction
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * AddOrderAction constructor.
     *
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param AddOrderRequest $request
     *
     * @return AddOrderResponse
     */
    public function execute(AddOrderRequest $request): AddOrderResponse
    {
        $order = new Order();
        $order->buyer_id = $request->getBuyerId();
        $order = $this->orderRepository->save($order);

        return new AddOrderResponse($order);
    }
}
