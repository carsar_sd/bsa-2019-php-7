<?php

namespace App\Http\Controllers;

use App\Action\AddOrderAction;
use App\Action\AddOrderRequest;

use App\Action\AddOrderItemAction;
use App\Action\AddOrderItemRequest;

use App\Http\Presenter\OrderArrayPresenter;
use App\Http\Presenter\OrderItemArrayPresenter;
use App\Http\Resources\OrderResource;

use App\Entity\Order;
use App\Entity\OrderItem;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    /**
     * @var OrderArrayPresenter
     */
    private $orderPresenter;
    /**
     * @var AddOrderAction
     */
    private $addOrderAction;
    /**
     * @var OrderItemArrayPresenter
     */
    private $orderItemPresenter;
    /**
     * @var AddOrderItemAction
     */
    private $addOrderItemAction;

    /**
     * OrderController constructor.
     *
     * @param OrderArrayPresenter     $orderPresenter
     * @param OrderItemArrayPresenter $orderItemPresenter
     * @param AddOrderAction          $addOrderAction
     * @param AddOrderItemAction      $addOrderItemAction
     */
    public function __construct(
        OrderArrayPresenter $orderPresenter,
        OrderItemArrayPresenter $orderItemPresenter,
        AddOrderAction $addOrderAction,
        AddOrderItemAction $addOrderItemAction
    ) {
        $this->orderPresenter     = $orderPresenter;
        $this->addOrderAction     = $addOrderAction;
        $this->orderItemPresenter = $orderItemPresenter;
        $this->addOrderItemAction = $addOrderItemAction;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();

        return $orders->map(function ($order) {
           return new OrderResource($order);
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Input::all();

        if (!isset($data['buyerId']) || !isset($data['orderItems'])) {
            throw new \InvalidArgumentException('Missing required fields');
        }

        /** -- order **/
        $order_response = $this->addOrderAction->execute(
            new AddOrderRequest($data['buyerId'])
        );
        $order_data = $this->orderPresenter->present($order_response->getOrder());
        /** // order **/

        if (!isset($order_data['id'])) {
            throw new \InvalidArgumentException('Missing order_id field');
        }

        /** -- order_item **/
        collect($data['orderItems'])->map(function ($order_item) use($order_data) {
            $this->addOrderItemAction->execute(
                new AddOrderItemRequest(
                    $order_data['id'],
                    $order_item['productId'],
                    $order_item['productQty'],
                    0,
                    $order_item['productDiscount'],
                    0.00
                )
            );
            //$this->orderItemPresenter->present($order_item_response->getOrderItem());
        });
        /** // order_item **/

        return new Response(['result' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new OrderResource(Order::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        if (!$order) return new Response([
            'result'  => 'fail',
            'message' => 'order not found'
        ]);

        $data = Input::all();

        if (!isset($data['orderId']) || !isset($data['orderItems'])) {
            throw new \InvalidArgumentException('Missing required fields');
        }

        if ($data['orderId'] != $id) {
            throw new \InvalidArgumentException('Incorrect orderId field');
        }

        /** -- order_item **/
        OrderItem::where('order_id', $id)->delete();
        collect($data['orderItems'])->map(function ($order_item) use($data) {
            $this->addOrderItemAction->execute(
                new AddOrderItemRequest(
                    $data['orderId'],
                    $order_item['productId'],
                    $order_item['productQty'],
                    0,
                    $order_item['productDiscount'],
                    0.00
                )
            );
            //$this->orderItemPresenter->present($order_item_response->getOrderItem());
        });
        /** // order_item **/

        return new Response(['result' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OrderItem::where('order_id', $id)->delete();
        $result = Order::destroy($id);

        return ['result' => $result ? 'success' : 'fail'];
    }
}
