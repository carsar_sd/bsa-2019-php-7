<?php

declare(strict_types = 1);

namespace App\Http\Presenter;

use App\Entity\Product;
use Illuminate\Support\Collection;

final class ProductArrayPresenter implements CollectionAsArrayPresenter
{
    /**
     * @var SellerArrayPresenter
     */
    private $sellerPresenter;

    /**
     * ProductArrayPresenter constructor.
     *
     * @param SellerArrayPresenter $sellerPresenter
     */
    public function __construct(SellerArrayPresenter $sellerPresenter)
    {
        $this->sellerPresenter = $sellerPresenter;
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    public function present(Product $product): array
    {
        return [
            'id'         => $product->getId(),
            'seller_id'  => $product->getSellerId(),
            'seller'     => $this->sellerPresenter->present($product->getSeller()),
            'name'       => $product->getName(),
            'price'      => $product->getPrice(),
            'available'  => $product->getAvailable(),
            'created_at' => $product->getCreatedAt()->toDateTimeString(),
            'updated_at' => $product->getUpdatedAt()->toDateTimeString(),
        ];
    }

    /**
     * @param Collection $collection
     *
     * @return array
     */
    public function presentCollection(Collection $collection): array
    {
        return $collection
            ->map(
                function (Product $product) {
                    return $this->present($product);
                }
            )
            ->all();
    }
}
