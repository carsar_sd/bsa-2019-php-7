<?php

declare(strict_types = 1);

namespace App\Http\Presenter;

use App\Entity\Seller;
use Illuminate\Support\Collection;

final class SellerArrayPresenter implements CollectionAsArrayPresenter
{
    /**
     * @param Seller $seller
     *
     * @return array
     */
    public function present(Seller $seller): array
    {
        return [
            'id'         => $seller->getId(),
            'name'       => $seller->getName(),
            'created_at' => $seller->getCreatedAt()->toDateTimeString(),
            'updated_at' => $seller->getUpdatedAt()->toDateTimeString(),
        ];
    }

    /**
     * @param Collection $collection
     *
     * @return array
     */
    public function presentCollection(Collection $collection): array
    {
        return $collection
            ->map(
                function (Seller $seller) {
                    return $this->present($seller);
                }
            )
            ->all();
    }
}
