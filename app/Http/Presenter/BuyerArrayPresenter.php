<?php

declare(strict_types = 1);

namespace App\Http\Presenter;

use App\Entity\Buyer;
use Illuminate\Support\Collection;

final class BuyerArrayPresenter implements CollectionAsArrayPresenter
{
    /**
     * @param Buyer $buyer
     *
     * @return array
     */
    public function present(Buyer $buyer): array
    {
        return [
            'id'         => $buyer->getId(),
            'name'       => $buyer->getName(),
            'surname'    => $buyer->getSurName(),
            'country'    => $buyer->getCountry(),
            'city'       => $buyer->getCity(),
            'address'    => $buyer->getAddress(),
            'created_at' => $buyer->getCreatedAt()->toDateTimeString(),
            'updated_at' => $buyer->getUpdatedAt()->toDateTimeString(),
        ];
    }

    /**
     * @param Collection $collection
     *
     * @return array
     */
    public function presentCollection(Collection $collection): array
    {
        return $collection
            ->map(
                function (Buyer $buyer) {
                    return $this->present($buyer);
                }
            )
            ->all();
    }
}
