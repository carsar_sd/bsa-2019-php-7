<?php

declare(strict_types = 1);

namespace App\Http\Presenter;

use App\Entity\Order;
use Illuminate\Support\Collection;

final class OrderArrayPresenter implements CollectionAsArrayPresenter
{
    /**
     * @var BuyerArrayPresenter
     */
    private $buyerPresenter;

    /**
     * OrderArrayPresenter constructor.
     *
     * @param BuyerArrayPresenter $buyerPresenter
     */
    public function __construct(BuyerArrayPresenter $buyerPresenter)
    {
        $this->buyerPresenter = $buyerPresenter;
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    public function present(Order $order): array
    {
        return [
            'id'         => $order->getId(),
            'buyer'      => $this->buyerPresenter->present($order->getBuyer()),
            'created_at' => $order->getCreatedAt()->toDateTimeString(),
            'updated_at' => $order->getUpdatedAt()->toDateTimeString(),
        ];
    }

    /**
     * @param Collection $collection
     *
     * @return array
     */
    public function presentCollection(Collection $collection): array
    {
        return $collection
            ->map(
                function (Order $order) {
                    return $this->present($order);
                }
            )
            ->all();
    }
}
