<?php

declare(strict_types = 1);

namespace App\Http\Presenter;

use App\Entity\OrderItem;
use Illuminate\Support\Collection;

final class OrderItemArrayPresenter implements CollectionAsArrayPresenter
{
    /**
     * @var ProductArrayPresenter
     */
    private $productPresenter;
    /**
     * @var OrderArrayPresenter
     */
    private $orderPresenter;

    /**
     * OrderItemArrayPresenter constructor.
     *
     * @param ProductArrayPresenter $productPresenter
     * @param OrderArrayPresenter   $orderPresenter
     */
    public function __construct(ProductArrayPresenter $productPresenter, OrderArrayPresenter $orderPresenter)
    {
        $this->productPresenter = $productPresenter;
        $this->orderPresenter   = $orderPresenter;
    }

    /**
     * @param OrderItem $order_item
     *
     * @return array
     */
    public function present(OrderItem $order_item): array
    {
        return [
            'id'         => $order_item->getId(),
            'product'    => $this->productPresenter->present($order_item->getProduct()),
            'order'      => $this->orderPresenter->present($order_item->getOrder()),
            'quantity'   => $order_item->getQuantity(),
            'price'      => $order_item->getPrice(),
            'discount'   => $order_item->getDiscount(),
            'amount'     => $order_item->getAmount(),
            'created_at' => $order_item->getCreatedAt()->toDateTimeString(),
            'updated_at' => $order_item->getCreatedAt()->toDateTimeString(),
        ];
    }

    /**
     * @param Collection $collection
     *
     * @return array
     */
    public function presentCollection(Collection $collection): array
    {
        return $collection
            ->map(
                function (OrderItem $order_item) {
                    return $this->present($order_item);
                }
            )
            ->all();
    }
}
