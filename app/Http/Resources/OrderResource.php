<?php

namespace App\Http\Resources;

use App\Entity\OrderItem;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'orderId'    => $this->id,
            'orderDate'  => $this->created_at->format('d.m.Y H:i:s'),
            'orderItems' => $items = OrderItemResource::collection(
                OrderItem::where('order_id', '=', $this->id)
                    ->orderBy('id')
                    ->get()
            ),
            'orderSum'   => collect($items)->sum('productSum'),
            'buyer' => [
                'buyerFullName' => $this->buyer->name,
                'buyerAddress' =>
                    $this->buyer->country . ', ' .
                    $this->buyer->city . ', ' .
                    $this->buyer->address,
                'buyerPhone' => $this->buyer->phone,
            ],
        ];
    }
}
